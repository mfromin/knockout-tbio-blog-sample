define(['plugins/router', 'durandal/app', 'dateFormat', 'textbox', 'textboxio', 'ko-validation'], function (router, app) {
    return {
        router: router,
        search: function() {
            //It's really easy to show a message box.
            //You can add custom options too. Also, it returns a promise for the user's response.
            app.showMessage('Search not yet implemented...');
        },
        activate: function () {
            router.map([
                { route: '', title:'Home', moduleId: 'viewmodels/home', nav: false },
                { route: 'about', title:'About', moduleId: 'viewmodels/about', nav: true },
                { route: 'contact', title:'Contact', moduleId: 'viewmodels/contact', nav: true },
                { route: 'post/:id', moduleId: 'viewmodels/post', nav:false},
                { route: 'createPost', moduleId: 'viewmodels/create', nav:false},
                { route: 'editPost/:id', moduleId: 'viewmodels/create', nav:false}
            ]).buildNavigationModel();

            return router.activate();
        }
    };
});
