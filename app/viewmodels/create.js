define(['knockout', 'plugins/router', 'tbio-extenders'], function (ko, router) {
	console.log('Create JS loaded!');

	var basePost = {
		"fullContent": ko.observable(""),
		//		"teaserContent": ko.observable("").extend({
		//			tbioRequiredField: "Field required",
		//			tbioMinLength: 3,
		//			tbioMaxLength: 2000
		//		}),
		"teaserContent": ko.observable(""),
		"imageURL": ko.observable(""),
		"tags": ko.observable(""),
		"author": ko.observable(""),
		"title": ko.observable(""),
		"category": {
			"_id": ko.observable("")
		}
	};

	function resetBasePost() {
		basePost.fullContent("");
		basePost.teaserContent("");
		basePost.imageURL("");
		basePost.tags("");
		basePost.author("");
		basePost.title("");
		basePost.category._id("");
	}

	var ctor = function () {
		console.log('CTOR function running');
		var self = this;
		var currentRouteTitle = router.activeInstruction().config.title;
		var serverPostData; //needed for edit post
		//console.log('Route title: ' + currentRouteTitle);
		self.categories = ko.observableArray([]);
		self.post = basePost;

		self.activate = function () {
			console.log('Activate running...');
			var categoriesUrl = 'http://localhost:8080/api/categories';
			jQuery.getJSON(categoriesUrl).done(function (data) {
				//console.log('Categories Data Fetched:' + JSON.stringify(data));
				self.categories(data);
			});

			if (currentRouteTitle === "CreatePost") {
				console.log('Creating post...');
				resetBasePost();
				self.saveButtonText = ko.observable('Save New Post');
				self.cancelButtonText = ko.observable('Cancel New Post');
				self.pageTitle = ko.observable("New Blog Post");
			} else { //EditPost
				console.log('Editing post...');
				self.saveButtonText = ko.observable('Update Post');
				self.cancelButtonText = ko.observable('Cancel Edit');
				self.pageTitle = ko.observable("Edit Blog Post");
				var postId = router.activeInstruction().params[0];
				var apiURL = "http://localhost:8080/api/posts/" + postId;
				return jQuery.getJSON(apiURL).done(function (data) {
					//console.log('Server Data Fetched');
					serverPostData = data;
					resetBasePost();
					var thisPost = basePost;
					thisPost.fullContent(data.fullContent);
					thisPost.teaserContent(data.teaserContent);
					thisPost.imageURL(data.imageURL);
					thisPost.tags(data.tags);
					thisPost.author(data.author);
					thisPost.title(data.title);
					thisPost.category._id(data.category._id);
					self.post = thisPost;
				});
			}
		};
		self.detached = function () {
			console.log('DETACHED');
		};

		//button methods for submit and cancel
		self.submitPost = function (model, event) {
			if (currentRouteTitle === "CreatePost") {
				model.postDate = new Date();
				var dataModel = ko.toJS(model);
				console.log('SUBMIT with data model: ' + dataModel);
				$.post('http://localhost:8080/api/posts', dataModel, function (response) {
					console.log('POST response: ' + JSON.stringify(response));
					$(location).attr('href', '#/');
				});
			} else { //EditPost
				//TODO: Put data back into the JSON and update last edited date!
				//console.log('Post data title:' + model.teaserContent());
				//console.log('Original data from server:' + JSON.stringify(serverPostData));
				serverPostData.fullContent = model.fullContent();
				serverPostData.teaserContent = model.teaserContent();
				serverPostData.imageURL = model.imageURL();
				serverPostData.tags = model.tags();
				serverPostData.author = model.author();
				serverPostData.title = model.title();
				serverPostData.category = {
					"_id": model.category._id()
				};
				console.log('JSON to send to server: ' + JSON.stringify(serverPostData));
				var postId = router.activeInstruction().params[0];
				$.ajax({
					url: 'http://localhost:8080/api/posts/' + postId,
					type: 'PUT',
					data: serverPostData,
					beforeSend: function (xhr) {
						console.log('BeforeSend function - null for now.');
					},
					success: function (response) {
						console.log('Success function with response: ' + response);
						$(location).attr('href', '#/');
					}
				});
			}
		}
	};

	//Note: This module exports a function. That means that you, the developer, can create multiple instances.
	//This pattern is also recognized by Durandal so that it can create instances on demand.
	//If you wish to create a singleton, you should export an object instead of a function.
	//See the "flickr" module for an example of object export.
	return ctor;
});
