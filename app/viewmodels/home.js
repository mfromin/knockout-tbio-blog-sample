define(['knockout'], function(ko) {
    var ctor = function () {
        var self = this;
        self.pageHeading = ko.observable("Fore Left!");
        self.secondaryText = ko.observable("  One Man's Search For Golf Nirvana");
        self.posts = ko.observable();

        var onlineURL = "http://localhost:8080/api/posts";
        console.log("Full URL:" + onlineURL);
        jQuery.getJSON(onlineURL).done(function (data) {
            //console.log('Server Data Fetched:' +  JSON.stringify(data));
            self.posts(data);
        });
    };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flick  r" module for an example of object export.

    return ctor;
});
