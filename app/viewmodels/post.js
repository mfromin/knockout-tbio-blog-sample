define(['knockout', 'plugins/router', 'moment', 'durandal/app', 'plugins/dialog'], function(ko, router, moment, app, dialog) {
    var ctor = function () {
        var self = this;
        self.message = ko.observable('Welcome to Single Post Page.');
        self.postId = router.activeInstruction().params[0];
        var apiURL = "http://localhost:8080/api/posts/" + self.postId;
        self.editPostUrl = "#/editPost/" + self.postId;
        self.singlePostData = ko.observable();
        self.tagsAsArray = function () {
            if (self.singlePostData().tags) {
                return self.singlePostData().tags.split(',');
            }
            return [];
        };
        self.activate = function () {
            //console.log('ACTIVATE called...');
            $(document).scrollTop(0);
        };
        console.log("Route ID:" + self.postId);
        jQuery.getJSON(apiURL).done(function (data) {
            //console.log('Server Data Fetched:' +  JSON.stringify(data));
            self.singlePostData(data);
        });
        //self.canDeactivate = function () {
        //    //the router's activator calls this function to see if it can leave the screen
        //    return app.showMessage('Are you sure you want to leave this page?', 'Navigate', ['Yes', 'No']);
        //};

        dialog.MessageBox.setViewUrl('/blog-durandal/app/views/deleteModal.html');

        self.confirmDelete = function () {
            app.showMessage("Do you really want to delete this post? This action cannot be 'undone'!", "Delete Confirmation", ['Yes', 'No'], false, { style:  { width: "600px"} }).then(function(dialogResult){
                //do something with the dialog result here
                console.log('Got this from the dialog ' + dialogResult);
            });

        };
    };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flickr" module for an example of object export.

    return ctor;
});
