requirejs.config({
	paths: {
		'jquery': '../public/bower_components/jquery/jquery',
		'text': '../public/bower_components/requirejs-text/text',
		'knockout': '../public/bower_components/knockout/dist/knockout.debug',
		"ko-validation": "../public/bower_components/knockout-validation/dist/knockout.validation",
		"moment": '../public/bower_components/moment/moment',
		"textboxio": '../public/bower_components/knockout-textbox.io/binding/textbox_binding',
		"tbio-configurations": "../public/bower_components/knockout-textbox.io/configurations/tbio-configurations",
		'durandal': '../lib/durandal/js',
		'plugins': '../lib/durandal/js/plugins',
		'transitions': '../lib/durandal/js/transitions',
		'bootstrap': '../lib/bootstrap/js/bootstrap',
		"dateFormat": '../app/bindings/dateFormat',
		"textbox": '../lib/textboxio/textboxio',
		"tbio-extenders": "../app/bindings/tbio-extenders"
	},
	shim: {
		'bootstrap': {
			deps: ['jquery'],
			exports: 'jQuery'
		}
	}
});

define(['durandal/system', 'durandal/app', 'durandal/viewLocator'], function (system, app, viewLocator) {
	//>>excludeStart("build", true);
	//system.debug(true);
	//>>excludeEnd("build");

	app.title = 'Textbox.io Sample Blog';

	app.configurePlugins({
		router: true,
		dialog: true
	});

	app.start().then(function () {
		//Replace 'viewmodels' in the moduleId with 'views' to locate the view.
		//Look for partial views in a 'views' folder in the root.
		viewLocator.useConvention();

		//Show the app by setting the root view model for our application with a transition.
		//app.setRoot('viewmodels/shell', 'entrance');
		app.setRoot('viewmodels/shell');
	});
});
