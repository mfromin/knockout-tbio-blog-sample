define(['knockout', 'moment'], function (ko, moment) {
    ko.bindingHandlers.date = {
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            var date = moment(value);
            $(element).text(date.format("LLL"));
        }
    };
});
