define(['knockout', 'jquery'], function (ko, $) {

	ko.extenders.tbioRequiredField = function (target, message) {
		function isEmpty(str) {
				return (!str || 0 === str.length);
			}
			//Add fields to the observable to address this validation
		target.hasRequiredError = ko.observable();
		target.requiredValidationMessage = ko.observable();
		if (!target.tbioValidation) {
			target.tbioValidation = [];
		}
		var validationObject = {
			errorType: "tbioRequiredField",
			hasError: ko.observable(false),
			validationMessage: ko.observable("tbioRequired Error message here")
		};
		target.tbioValidation.push(validationObject);

		function validate(newValue) {
				var jStrippedString = $(newValue).text().trim();
				//console.log('Stripped string: ' + jStrippedString);
				//console.log('Is stripped string EMPTY?: ' + isEmpty(jStrippedString));
				if (isEmpty(jStrippedString)) {
					target.hasRequiredError(true);
					target.requiredValidationMessage("This is a required field - please enter a value");
					console.log("tbioRequiredField validation error: TRUE");
				} else {
					target.hasRequiredError(false);
					target.requiredValidationMessage("");
					console.log("tbioRequiredField validation error: FALSE");
				}
			}
			//validate(target());  //don't run on page load!
		target.subscribe(validate);
		return target;
	};

	ko.extenders.tbioMinLength = function (target, minLength) {
		//Add fields to the observable to address this validation
		target.hasMinError = ko.observable();
		target.minValidationMessage = ko.observable();

		if (!target.tbioValidation) {
			target.tbioValidation = [];
		}
		var validationObject = {
			errorType: "tbioMinLength",
			hasError: ko.observable(true),
			validationMessage: ko.observable("tbioMinLength Error message here")
		}
		target.tbioValidation.push(validationObject);

		function validate(newValue) {
				var jStrippedString = $(newValue).text().trim();
				if (jStrippedString.length >= minLength) {
					target.hasMinError(false);
					target.minValidationMessage("");
					console.log("tbioMinLength validation error: FALSE");
				} else {
					target.hasMinError(true);
					target.minValidationMessage("fewer than the minimum of " + minLength + " characters.");
					console.log("tbioMinLength validation error: TRUE");
				}
			}
			//validate(target());  //don't run on page load!
		target.subscribe(validate);
		return target;
	};

	ko.extenders.tbioMaxLength = function (target, maxLength) {
		//Add fields to the observable to address this validation
		if (!target.tbioValidation) {
			target.tbioValidation = [];
		}
		var validationObject = {
			errorType: "tbioMaxLength",
			hasError: ko.observable(true),
			validationMessage: ko.observable("tbioMaxLength Error message here")
		}
		target.tbioValidation.push(validationObject);

		function validate(newValue) {
				var jStrippedString = $(newValue).text().trim();
				if (jStrippedString.length <= maxLength) {
					target.hasError(false);
					target.maxValidationMessage("");
					console.log("tbioMaxLength validation error: FALSE");
				} else {
					target.hasError(true);
					target.maxValidationMessage("greater than the maximum of " + maxLength + " characters.");
					console.log("tbioMaxLength validation error: TRUE");
				}
			}
			//validate(target());  //don't run on page load!
		target.subscribe(validate);
		return target;
	};
});
